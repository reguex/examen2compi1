//
// Created by Doninelli on 12/14/17.
//

#ifndef SRC_AST_H
#define SRC_AST_H


#include <string>
#include <memory>
#include <vector>
#include <list>
#include <map>

#define BLOCK_STATEMENT "BLOCK"

using namespace std;



class Object
{
public:

    virtual std::string toString() = 0;
};


class StringObj : public Object {
public:
    std::string value;

    StringObj(std::string value);

    std::string toString() override;
};

class IntObj : public Object {
public:
    int value;

    IntObj(int value);

    std::string toString() override;
};


class VarObj : public Object {
public:
    std::string value;

    explicit VarObj(const std::string &value);

    std::string toString() override;
};

class None : public Object {
public:
    std::string toString() override;
};

class BoolObj : public Object {
public:
    bool value;

    BoolObj(bool value);

    std::string toString() override;
};


class VariableTable
{
    std::shared_ptr<std::map<std::string, std::shared_ptr<Object>>> variables;

public:
    VariableTable();

    void set(std::string key, std::shared_ptr<Object> value);

    std::shared_ptr<Object> find(std::string var);
};


class Expr
{

protected:
    [[noreturn]] void raiseInvalidTypeError(std::string type);

public:
    virtual shared_ptr<Object> execute() = 0;

    virtual std::string getKind();
};

typedef std::vector<Expr *> ExprList;

class Expression : public Expr
{
public:
    shared_ptr<Object> object;

    Expression(const shared_ptr<Object> &object);

    Expression();
};

class Statement : public Expr
{

};

class IdExpr : public Expression
{
private:
    string varName;

public:
    explicit IdExpr(std::string name);

    shared_ptr<Object> execute() override;

    const string &getVarName() const;
};

class NumExpr : public Expression
{
public:
    NumExpr(int value);

    shared_ptr<Object> execute() override;
};

class StringExpr : public Expression
{
public:
    StringExpr(string value);

    shared_ptr<Object> execute() override;
};

class ModExpr : public Expression
{
private:
    Expr *left;
    Expr *right;

public:
    ModExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};

class DivExpr : public Expression
{
private:
    Expr *left;
    Expr *right;

public:
    DivExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};

class MultExpr : public Expression
{
private:
    Expr *left;
    Expr *right;

public:
    MultExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};

class SubExpr : public Expression
{
private:
    Expr *left;
    Expr *right;

public:
    SubExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};

class AddExpr : public Expression
{
private:
    Expr *left;
    Expr *right;

public:
    AddExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};

class EQExpr : public Expression
{
private:
    Expr *left;
    Expr *right;

public:
    EQExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};

class NEExpr : public Expression
{
private:
    Expr *left;
    Expr *right;
public:
    NEExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};

class GTEExpr : public Expression
{
private:
    Expr *left;
    Expr *right;
public:
    GTEExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};


class LTEExpr : public Expression
{
private:
    Expr *left;
    Expr *right;
public:
    LTEExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};


class LTExpr : public Expression
{
private:
    Expr *left;
    Expr *right;
public:
    LTExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};


class GTExpr : public Expression
{
private:
    Expr *left;
    Expr *right;
public:
    GTExpr(Expr *left, Expr *right);

    shared_ptr<Object> execute() override;
};


class WritelnStatement : public Statement
{
private:
    ExprList exprs;

public:
    explicit WritelnStatement(ExprList exprs);

    shared_ptr<Object> execute() override;
};


class BlockStatement : public Statement
{
public:
    list<Statement *> stList;
public:
    explicit BlockStatement(list<Statement *> exprs);

    shared_ptr<Object> execute() override;

    string getKind() override;

};


class IfStatement : public Statement {
public:
    Expr *condition;
    Statement *ifBlock;
    Statement *elseBlock;

    IfStatement(Expr *condition, Statement *ifBlock, Statement *elseBlock);

    shared_ptr<Object> execute() override;
};


class AssignStatement : public Statement {
public:
    string id;
    Expr *expr;

    AssignStatement(string id, Expr *expr);

    shared_ptr<Object> execute() override;
};

class WhileStatement : public Statement {
private:
    Expr *condition;
    Statement *block;

public:
    WhileStatement(Expr *condition, Statement *block);

    shared_ptr<Object> execute() override;
};

#endif //SRC_AST_H
