//
// Created by Doninelli on 12/14/17.
//

#include <iostream>
#include "ast.h"

using namespace std;


StringObj::StringObj(std::string value) : value(value)
{}

std::string StringObj::toString()
{
    return value;
}

IntObj::IntObj(int value) : value(value)
{}

std::string IntObj::toString()
{
    return std::to_string(value);
}

VarObj::VarObj(const std::string &value) : value(value)
{}

std::string VarObj::toString()
{
    return "Var(" + value + ")";
}

std::string None::toString()
{
    return "";
}

BoolObj::BoolObj(bool value) : value(value)
{}

std::string BoolObj::toString()
{
    return std::to_string(value);
}



auto table = make_shared<VariableTable>(VariableTable());

VariableTable::VariableTable()
{
    variables = make_shared<map<string, shared_ptr<Object>>>(map<string, shared_ptr<Object>>());
};


void VariableTable::set(string key, shared_ptr<Object> value)
{
    if (variables->find(key) != variables->end())
    {
        variables->at(key) = value;
    }
    else
    {
        variables->insert(pair<string, shared_ptr<Object>>(key, value));
    }
}

shared_ptr<Object> VariableTable::find(string var)
{
    if (variables->find(var) != variables->end())
    {
        return variables->at(var);
    }
    else
    {
        return make_shared<None>(None());
    }
}

Expression::Expression(const shared_ptr<Object> &object) : object(object)
{}

Expression::Expression()
{
    this->object = make_shared<None>(None());
}


IdExpr::IdExpr(std::string name) : Expression(make_shared<VarObj>(name)), varName(name)
{
}

shared_ptr<Object> IdExpr::execute()
{
    return table->find(varName);
}

const string &IdExpr::getVarName() const
{
    return varName;
}

NumExpr::NumExpr(int value) : Expression(make_shared<IntObj>(value))
{}

shared_ptr<Object> NumExpr::execute()
{
    return object;
}

StringExpr::StringExpr(string value) : Expression(make_shared<StringObj>(value))
{}

shared_ptr<Object> StringExpr::execute()
{
    return object;
}


ModExpr::ModExpr(Expr *left, Expr *right) : left(left), right(right)
{}

shared_ptr<Object> ModExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<IntObj>(IntObj(l->value % r->value));
        }
    }

    raiseInvalidTypeError("int");
}

void Expr::raiseInvalidTypeError(std::string type)
{
    throw std::invalid_argument("Invalid type. Expected: " + type);
}

std::string Expr::getKind()
{
    return "NOTHING";
}

DivExpr::DivExpr(Expr *left, Expr *right) : left(left), right(right)
{}

shared_ptr<Object> DivExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<IntObj>(IntObj(l->value / r->value));
        }
    }

    raiseInvalidTypeError("int");
}

MultExpr::MultExpr(Expr *left, Expr *right) : left(left), right(right)
{}

shared_ptr<Object> MultExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<IntObj>(IntObj(l->value * r->value));
        }
    }

    raiseInvalidTypeError("int");
}

SubExpr::SubExpr(Expr *left, Expr *right) : left(left), right(right)
{}

shared_ptr<Object> SubExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<IntObj>(IntObj(l->value - r->value));
        }
    }

    raiseInvalidTypeError("int");
}

AddExpr::AddExpr(Expr *left, Expr *right) : left(left), right(right)
{

}

shared_ptr<Object> AddExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<IntObj>(IntObj(l->value + r->value));
        }
    }

    raiseInvalidTypeError("int");
}

EQExpr::EQExpr(Expr *left, Expr *right) : left(left), right(right)
{
}

shared_ptr<Object> EQExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value == r->value));
        }
        else
        {
            raiseInvalidTypeError("int");
        }
    }
    else if (auto l = dynamic_pointer_cast<StringObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<StringObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value == r->value));
        }
        else
        {
            raiseInvalidTypeError("string");
        }
    }
    else if (auto l = dynamic_pointer_cast<BoolObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<BoolObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value == r->value));
        }
        else
        {
            raiseInvalidTypeError("bool");
        }
    }

    raiseInvalidTypeError("int, string, bool");
}

NEExpr::NEExpr(Expr *left, Expr *right) : left(left), right(right)
{}

shared_ptr<Object> NEExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value != r->value));
        }
        else
        {
            raiseInvalidTypeError("int");
        }
    }
    else if (auto l = dynamic_pointer_cast<StringObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<StringObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value != r->value));
        }
        else
        {
            raiseInvalidTypeError("string");
        }
    }
    else if (auto l = dynamic_pointer_cast<BoolObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<BoolObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value != r->value));
        }
        else
        {
            raiseInvalidTypeError("bool");
        }
    }

    raiseInvalidTypeError("int, string, bool");
}

GTEExpr::GTEExpr(Expr *left, Expr *right) : left(left), right(right)
{
}

shared_ptr<Object> GTEExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value >= r->value));
        }
    }

    raiseInvalidTypeError("int");
}

LTEExpr::LTEExpr(Expr *left, Expr *right) : left(left), right(right)
{

}

shared_ptr<Object> LTEExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value <= r->value));
        }
    }

    raiseInvalidTypeError("int");
}

LTExpr::LTExpr(Expr *left, Expr *right) : left(left), right(right)
{
}

shared_ptr<Object> LTExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value < r->value));
        }
    }

    raiseInvalidTypeError("int");
}

GTExpr::GTExpr(Expr *left, Expr *right) : left(left), right(right)
{
}

shared_ptr<Object> GTExpr::execute()
{
    if (auto l = dynamic_pointer_cast<IntObj>(left->execute()))
    {
        if (auto r = dynamic_pointer_cast<IntObj>(right->execute()))
        {
            return make_shared<BoolObj>(BoolObj(l->value > r->value));
        }
    }

    raiseInvalidTypeError("int");
}

WritelnStatement::WritelnStatement(ExprList exprs) : exprs(exprs)
{}

shared_ptr<Object> WritelnStatement::execute()
{
    for (auto expr : exprs)
    {
        std::cout << expr->execute()->toString() << " ";
    }

    std::cout << std::endl;

    return make_shared<None>(None());
}

BlockStatement::BlockStatement(list<Statement *> exprs) : stList(exprs)
{}

shared_ptr<Object> BlockStatement::execute()
{
    for (auto e : stList)
    {
        e->execute();
    }

    return make_shared<None>(None());
}

string BlockStatement::getKind()
{
    return BLOCK_STATEMENT;
}

IfStatement::IfStatement(Expr *condition, Statement *ifBlock, Statement *elseBlock) : condition(condition),
                                                                                      ifBlock(ifBlock),
                                                                                      elseBlock(elseBlock)
{}

shared_ptr<Object> IfStatement::execute()
{
    if (auto result = dynamic_pointer_cast<BoolObj>(condition->execute()))
    {
        if (result->value)
        {
            ifBlock->execute();
        }
        else
        {
            elseBlock->execute();
        }
    }
    else
    {
        std::cout << elseBlock->execute()->toString();
        
        raiseInvalidTypeError("bool");
    }

    return make_shared<None>(None());
}

AssignStatement::AssignStatement(string id, Expr *expr) : id(id), expr(expr)
{}

shared_ptr<Object> AssignStatement::execute()
{
    table->set(id, expr->execute());

    return make_shared<None>(None());
}

WhileStatement::WhileStatement(Expr *condition, Statement *block) : condition(condition), block(block)
{}

shared_ptr<Object> WhileStatement::execute()
{
    while (true) {
        if (auto conditionEval = dynamic_pointer_cast<BoolObj>(condition->execute())) {
            if (conditionEval->value) {
                block->execute();
            } else {
                break;
            }
        } else {
            raiseInvalidTypeError("bool");
        }
    }

    return make_shared<None>(None());
}
